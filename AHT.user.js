// ==UserScript==
// @name         AHT
// @namespace    https://discord.gg/PGcJfpH
// @version      0.1
// @description  try to take over the world!
// @author       Helmit
// @match        http://*/*
// @match        https
// @require            https://openuserjs.org/src/libs/sizzle/GM_config.js
// @grant              GM_getValue
// @grant              GM_setValue
// ==/UserScript==

(function() {

    function arr_diff (a1, a2) {
    	var a = [], diff = [];
    	for (var i = 0; i < a1.length; i++) {
    			a[a1[i]] = true;
    	}
    	for (i = 0; i < a2.length; i++) {
    			if (a[a2[i]]) {
    					delete a[a2[i]];
    			} else {
    					a[a2[i]] = true;
    			}
    	}
    	for (var k in a) {
    			diff.push(k);
    	}
    	return diff;
    }
    var weirdHappend = {
    	"haxclick":{},
    	"vina-full":{
    		"0":[12, 16, 40, 44, 48, 52, 68, 72, 84, 88, 96, 100, 120, 124, 128, 132, 152, 156, 160, 164, 184, 188, 192, 196, 216, 220, 228, 232, 244, 248, 264, 268, 272, 276, 300, 304],
    		"1":[12, 16, 40, 44, 48, 68, 72, 76, 80, 108, 112, 140, 144, 172, 176, 204, 208, 236, 240, 268, 272, 292, 296, 300, 304, 308, 312],
    		"2":[8, 12, 16, 20, 36, 40, 52, 56, 64, 68, 88, 92, 120, 124, 148, 152, 176, 180, 204, 208, 232, 236, 260, 264, 288, 292, 296, 300, 304, 308, 312, 316],
    		"3":[4, 8, 12, 16, 20, 32, 36, 52, 56, 88, 92, 116, 120, 140, 144, 148, 180, 184, 216, 220, 248, 252, 256, 260, 276, 280, 292, 296, 300, 304, 308],
    		"4":[20, 24, 48, 52, 56, 76, 80, 84, 88, 104, 108, 116, 120, 132, 136, 148, 152, 160, 164, 180, 184, 192, 196, 200, 204, 208, 212, 216, 220, 244, 248, 276, 280, 308, 312],
    		"5":[0, 4, 8, 12, 16, 20, 24, 32, 36, 64, 68, 96, 100, 108, 112, 116, 128, 132, 136, 148, 152, 184, 188, 216, 220, 224, 228, 248, 252, 260, 264, 276, 280, 296, 300, 304, 308],
    		"6":[8, 12, 16, 20, 36, 40, 52, 56, 64, 68, 88, 96, 100, 128, 132, 140, 144, 148, 160, 164, 168, 180, 184, 192, 196, 216, 220, 224, 228, 248, 252, 260, 264, 276, 280, 296, 300, 304, 308],
    		"7":[0, 4, 8, 12, 16, 20, 24, 28, 56, 60, 88, 92, 116, 120, 144, 148, 172, 176, 200, 204, 228, 232, 256, 260, 288, 292],
    		"8":[8, 12, 16, 20, 36, 40, 52, 56, 64, 68, 88, 92, 100, 104, 116, 120, 136, 140, 144, 148, 164, 168, 180, 184, 192, 196, 216, 220, 224, 228, 248, 252, 260, 264, 276, 280, 296, 300, 304, 308],
    		"9":[8, 12, 16, 20, 36, 40, 52, 56, 64, 68, 88, 92, 96, 100, 120, 124, 132, 136, 148, 152, 156, 168, 172, 176, 184, 188, 216, 220, 228, 248, 252, 260, 264, 276, 280, 296, 300, 304, 308],
    		"a":[104, 108, 112, 116, 120, 132, 136, 152, 156, 184, 188, 196, 200, 204, 208, 212, 216, 220, 224, 228, 248, 252, 256, 260, 276, 280, 284, 292, 296, 300, 304, 312, 316],
    		"b":[0, 4, 32, 36, 64, 68, 96, 100, 108, 112, 116, 128, 132, 136, 148, 152, 160, 164, 184, 188, 192, 196, 216, 220, 224, 228, 248, 252, 256, 260, 264, 276, 280, 288, 292, 300, 304, 308],
    		"c":[104, 108, 112, 116, 120, 132, 136, 152, 156, 160, 164, 192, 196, 224, 228, 260, 264, 280, 284, 296, 300, 304, 308, 312],
    		"d":[24, 28, 56, 60, 88, 92, 104, 108, 112, 120, 124, 132, 136, 148, 152, 156, 160, 164, 184, 188, 192, 196, 216, 220, 224, 228, 248, 252, 260, 264, 276, 280, 284, 296, 300, 304, 312, 316],
    		"e":[104, 108, 112, 116, 132, 136, 148, 152, 160, 164, 184, 188, 192, 196, 200, 204, 208, 212, 216, 220, 224, 228, 260, 264, 280, 284, 296, 300, 304, 308, 312],
    		"f":[12, 16, 20, 24, 40, 44, 56, 60, 72, 76, 88, 92, 104, 108, 136, 140, 160, 164, 168, 172, 176, 180, 200, 204, 232, 236, 264, 268, 296, 300]
    	}
    };
    function solveCaptcha(theCaptcha,cords=[[10,18],[28,36],[46,54],[64,72]],dataName="vina-full"){
    	var canvas = document.createElement("canvas");
    	canvas.setAttribute("width", theCaptcha.naturalWidth);
    	canvas.setAttribute("height", theCaptcha.naturalHeight);
    	canvas.style="top:0px;left:0px;position:fixed;";
    	document.body.appendChild(canvas);
    	var ctx = canvas.getContext('2d');
    	ctx.drawImage(theCaptcha, 0, 0);
    	var imageData = ctx.getImageData(0,0,theCaptcha.naturalWidth, theCaptcha.naturalHeight);
    	var data = imageData.data;
    	var bgColors=[];
    	for(var i=0;i<data.length;i+=4){
    		var x = i % theCaptcha.naturalWidth;
    		var y = parseInt(i/theCaptcha.naturalHeight);

    		if(bgColors.length<2&&bgColors.indexOf(data[i]+","+data[i+1]+","+data[i+2])==-1){
    			bgColors.push(data[i]+","+data[i+1]+","+data[i+2])
    		}
    		if(bgColors.indexOf(data[i]+","+data[i+1]+","+data[i+2])!=-1){
    			data[i]=255;
    			data[i+1]=255;
    			data[i+2]=255;
    		} else {
    			data[i]=0;
    			data[i+1]=0;
    			data[i+2]=0;
    		}
    	}
    	ctx.putImageData(imageData,0,0);
    	var result="";
    	for(var cord=0;cord<cords.length;cord++){
    		var imageData = ctx.getImageData(cords[cord][0],10,8,10);
    		var data = imageData.data;
    		var output=[];
    		for(var i=0;i<data.length;i+=4){
    			if(data[i]==0&&data[i+1]==0&&data[i+2]==0){
    				output.push(i);
    			}
    		}
    		for(var key in weirdHappend[dataName]){
    			if(arr_diff(weirdHappend[dataName][key],output).length==0){
    				result+=key;
    				break;
    			}
    		}
    	}
    	return result;
    }
    var dataHack;
    function beforeLoad(){
        window.location.href="/adStart";
    }
    function form(){
        setTimeout(function(){
            try{
                var container = document.querySelector("#ActionForm");
                var captchaImage = container.querySelector('img[src="/assets/php/Captcha.php"]');
                if(container.querySelector('a[href="/redirect.html"]') && container.querySelector('a[href="/redirect.html"]').style.display!="none"){
                    container.querySelector('a[href="/redirect.html"]').click();
                } else{
                    if(container.querySelector("#showInfo").checked==false){
                        container.querySelector("#showInfo").click();
                    }
                    var dataHack=GM_getValue("aht-data");
                    console.log(dataHack);
                    for(var k in dataHack){
                        try{
                            if(k!="hackURL"){
                                container.querySelectorAll('[name="'+k+'"]')[0].value = dataHack[k];
                            }
                        }catch(e){
                        }
                    }
                    container.querySelector('input[name="captchaKey"]').value=solveCaptcha(captchaImage);
                    var clickdaShit=setInterval(function(){
                        var result = document.querySelector("body > div.wrapper > div.content-wrapper > section > div.row > div.col-lg-4 > div > div.box-body.dlHackInfo").textContent;
                        document.querySelector('input[value="Submit"]').click();
                        var check=setInterval(function(){
                            if(result !=  document.querySelector("body > div.wrapper > div.content-wrapper > section > div.row > div.col-lg-4 > div > div.box-body.dlHackInfo").textContent){
                                clearInterval(clickdaShit);
                                clearInterval(check);
                                container.querySelector('a[href="/redirect.html"]').click();
                            }
                        },500);
                    },2000);
                }
            }catch(e){
                alert(e.message);
            }
        },2500);
    }
    function init(){
        if(document.URL==="https://dc4u.eu/games/dragon-city"){
            var container = document.querySelector("#ActionForm");
            container.setAttribute("novalidate","true");
            if(GM_getValue("aht-started")===true){
                var captchaImage = container.querySelector('img[src="/assets/php/Captcha.php"]');
                if(captchaImage.complete){
                    form();
                } else {
                    captchaImage.onload=function(){
                        form();
                    }
                }
            } else {
                var newInput=document.createElement("button");
                newInput.textContent="Save Data and Start";
                newInput.setAttribute("class","btn btn-primary");
                newInput.onclick=function(){
                    var result={};
                    var blocks = container.getElementsByClassName("form-group");
                    for(var i=0;i<blocks.length;i++){
                        try{
                            if(blocks[i].parentElement.style.display==="" && blocks[i].style.display===""){
                                var obj=blocks[i].children[1];
                                result[obj.getAttribute("name")]=obj.value;
                            }
                        }catch(e){}
                    }
                    GM_setValue("aht-data",result);
                    GM_setValue("aht-started",true);
                    location.reload(true);
                };
                container.appendChild(newInput);
            }
        }
    }

    function appendStop(){
        var a1=document.createElement("button");
        a1.textContent="Stop AHT";
        a1.style="position:fixed;top:0px;right:0px;";
        a1.onclick=function(){
            GM_setValue("aht-started",false);
        }
        document.body.appendChild(a1);
    }
    var ahtstarted = GM_getValue("aht-started");
    switch(document.domain){
        case "dc4u.eu":
            if(ahtstarted && document.URL==="https://dc4u.eu/redirect.html"){
                beforeLoad();
            } else {
                document.onreadystatechange = function () {
                    if(document.readyState==="interactive"){
                        if(ahtstarted){appendStop()}
                        init();
                    }
                }
            }
            break;
        default:
            if(ahtstarted && (document.domain === "123link.pw" || document.domain === "ouo.io" || document.domain === "ouo.press")){
                window.history.back();
            }
            break;
    }
})();
